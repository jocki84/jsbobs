// Numbers refer to the Promises/A+ specification at https://promisesaplus.com
(function(f){if('undefined'!==typeof module)module.exports=f();else this.JPromise=f();})
(function () {
    'use strict';

    var PENDING = 'pending';
    var FULFILLED = 'fulfilled';
    var REJECTED = 'rejected';

    /**
     * If func is callable, return an invoker that calls resolve or reject as
     * appropriate (see 2.2.7).
     *
     * Otherwise, return resolve or reject themselves depending on forState.
     */
    function actionFor(forState, func, resolve, reject) {
        if ('function' === typeof func) {
            return function (val) {
                try {
                    // 2.2.5
                    resolve(func(val));
                } catch (reason) {
                    reject(reason);
                }
            };
        } else if (FULFILLED === forState) {
            // 2.2.7.3
            return resolve;
        } else {
            // 2.2.7.4
            return reject;
        }
    }

    /**
     * Wrap the given functions into resolution functions that allow at most
     * one of the functions to be called at most once.
     */
    function resolutionFunctions(resolve, reject) {
        var alreadyCalled = false;

        function callOnce(func) {
            return function (arg) {
                // 2.3.3.3.3
                if (alreadyCalled) {
                    return;
                }
                alreadyCalled = true;
                return func(arg);
            };
        }

        return [ callOnce(resolve), callOnce(reject) ];
    }

    function JPromise(executor) {
        if (!(this instanceof JPromise)) {
            throw new Error('JPromise() must not be called as a function.');
        }

        var self = this;
        var m_actions = {};
        var m_state = PENDING;
        var m_value;

        m_actions[FULFILLED] = [];
        m_actions[REJECTED] = [];

        function addAction(forState, func, resolve, reject) {
            var action = actionFor(forState, func, resolve, reject);
            if (PENDING === m_state) {
                m_actions[forState].push(action);
            } else if (forState === m_state) {
                // 2.2.4
                setTimeout(function () { action(m_value); }, 0);
            }
        }

        this.then = function then(onFulfilled, onRejected) {
            if ((REJECTED === m_state || 'function' !== typeof onFulfilled) &&
                (FULFILLED === m_state || 'function' !== typeof onRejected))
            {
                return this;
            }

            return new JPromise(function (resolve, reject) {
                addAction(FULFILLED, onFulfilled, resolve, reject);
                addAction(REJECTED, onRejected, resolve, reject);
            });
        };

	/**
	 * This function must only be called if PENDING === m_state.
	 */
        function settle(state, value) {
            m_state = state;
            m_value = value;

            var theActions = m_actions[state];
            delete m_actions[FULFILLED];
            delete m_actions[REJECTED];

            // 2.2.4
            setTimeout(function () {
                // 2.2.2, 2.2.3, 2.2.5, 2.2.6
                theActions.forEach(function (action) { action(value); });
            }, 0);
        }

        /**
         * The mechanism in resolutionFunctions ensures that this function and
         * the correcsponding resolve function can only be called at most once
         * from client code.
         *
         * Thus, this Promise's state is guaranteed to be PENDING when this
         * function is called. The Promise transitions to REJECTED with the
         * given reason.
         */
        function reject(reason) {
            settle(REJECTED, reason);
        }

        /**
         * The mechanism in resolutionFunctions ensures that this function and
         * the correcsponding reject function can only be called at most once
         * from client code.
         *
         * Thus, this Promise's state is guaranteed to be PENDING when this
         * function is called. The Promise either transitions to FULFILLED or
         * REJECTED or a new set of resolution functions are handed to a
         * thenable to allow calling resolve or reject one more time.
         */
        function resolve(value) {
            var then;

            // 2.3.1
            if (self === value) {
                return settle(REJECTED, new TypeError('JPromise cannot resolve to itself.'));
            }

            // 2.3.4
            if ('object' !== typeof value && 'function' !== typeof value ||
                null === value)
            {
                return settle(FULFILLED, value);
            }

            try {
                // 2.3.3.1
                then = value.then;
            } catch (reason) {
                // 2.3.3.2
                return settle(REJECTED, reason);
            }

            // 2.3.3.4
            if ('function' !== typeof then) {
                return settle(FULFILLED, value);
            }

            var resolution = resolutionFunctions(resolve, reject);
            try {
                // 2.3.3.3
                then.apply(value, resolution);
            } catch (reason) {
                // 2.3.3.3.4.1, 2.3.3.3.4.2
                return resolution[1](reason);
            }
        }

        executor.apply(void 0, resolutionFunctions(resolve, reject));
    }

    JPromise.prototype['catch'] = function catch_(onRejected) {
        return this.then(void 0, onRejected);
    };

    JPromise.resolve = function resolve(value) {
        if (value instanceof JPromise) {
            return value;
        }
        return new JPromise(function (resolve, reject) {
            resolve(value);
        });
    };

    JPromise.reject = function reject(reason) {
        return new JPromise(function (resolve, reject) {
            reject(reason);
        });
    };

    JPromise.race = function (promises) {
        return new JPromise(function (resolve, reject) {
            promises.forEach(function (promise) {
                promise.then(resolve, reject);
            });
        });
    };

    JPromise.all = function (promises) {
        return new JPromise(function (resolve, reject) {
            var count = 0;
            var results = [];
            promises.forEach(function (promise, i) {
                ++count;
                promise.then(function (value) {
                    --count;
                    results[i] = value;
                    if (0 === count) {
                        resolve(results);
                    }
                },
                reject);
            });
        });
    };

    return JPromise;
});
