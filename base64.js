/*
 * This file adds messages to class String that handle Base64 encoding
 * described in RFC 2045, section 6.8, see
 * <http://http://www.rfc-editor.org/rfc/rfc2045.txt>.
 */

function Base64() {
}

Base64.A = 'A'.charCodeAt(0)
Base64.a = 'a'.charCodeAt(0)
Base64.zero = '0'.charCodeAt(0)
Base64.PAD = 64
Base64.chars = '+/='

String.forBase64Code = function(c) {
	if (c < 26)
		return String.fromCharCode(Base64.A + c)
	c -= 26
	if (c < 26)
		return String.fromCharCode(Base64.a + c)
	c -= 26
	if (c < 10)
		return String.fromCharCode(Base64.zero + c)
	return Base64.chars.charAt(c - 10)
}

String.prototype.base64CodeAt = function(index) {
	var i = Base64.chars.indexOf(this.charAt(index))
	if (i != -1)
		return 62 + i
	var c = this.charCodeAt(index) - Base64.zero
	if (c >= 0 && c < 10)
		return c + 52
	c -= Base64.a - Base64.zero
	if (c >= 0 && c < 26)
		return c + 26
	c -= Base64.A - Base64.a
	if (c >= 0 && c < 26)
		return c
	return -1
}

String.CHARMASK = (1 << 8) - 1

String.prototype.encode_base64 = function() {
	var r = ''
	for (var i = 0; i < this.length; i += 3) {
		var x, c = [Base64.PAD, Base64.PAD, Base64.PAD, Base64.PAD]
		x = this.charCodeAt(i) & String.CHARMASK
		c[0] = x >> 2
		c[1] = (x & 3) << 4
		if (i + 1 < this.length) {
			x = this.charCodeAt(i + 1) & String.CHARMASK
			c[1] += x >> 4
			c[2] = (x & 15) << 2
		}
		if (i + 2 < this.length) {
			x = this.charCodeAt(i + 2) & String.CHARMASK
			c[2] += x >> 6
			c[3] = x & 63
		}
		for (var j = 0; j != 4; j++)
			r = r.concat(String.forBase64Code(c[j]))
		if ((i + 3) % 54 == 0)
			r = r.concat("\n")
	}
	return r
}

String.prototype.decode_base64 = function() {
	var r = '', t = [], j = 0
	for (var i = 0; i != this.length; i++) {
		t[j] = this.base64CodeAt(i)
		if (t[j] != -1 && ++j == 4) {
			var a = (t[0] << 2) + (t[1] >> 4)
			var b = ((t[1] & 15) << 4) + (t[2] >> 2)
			var c = ((t[2] & 3) << 6) + t[3]
			r = r.concat(String.fromCharCode(a))
			if (t[2] != Base64.PAD)
				r = r.concat(String.fromCharCode(b))
			if (t[3] != Base64.PAD)
				r = r.concat(String.fromCharCode(c))
			j = 0
		}
	}
	return r
}
