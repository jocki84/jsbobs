/*
 * This file defines class BitIO which supports reading and writing words
 * with a varying number of bits to and from a String.
 */

function BitIO() {
}

BitIO.CHARBITS = 8
BitIO.CHARMASK = (1 << BitIO.CHARBITS) - 1
BitIO.prototype.data = ''
BitIO.prototype.pos = 0
BitIO.prototype.bitpos = 0
BitIO.prototype.word = 0

BitIO.prototype.append = function(code, bits) {
	if (this.bitpos != 0) {
		code = (code << this.bitpos) | this.word
		bits += this.bitpos
	}
	while (bits >= BitIO.CHARBITS) {
		this.data = this.data.concat(
			String.fromCharCode(code & BitIO.CHARMASK))
		code >>= BitIO.CHARBITS
		bits -= BitIO.CHARBITS
	}
	this.word = code
	this.bitpos = bits
}

BitIO.prototype.close = function() {
	if (this.bitpos != 0)
		this.append(0, BitIO.CHARBITS - this.bitpos)
}

BitIO.prototype.source = function(s) {
	this.data = s.toString()
	this.pos = 0
}

BitIO.prototype.get = function(bits) {
	var r = this.word
	var i = this.bitpos
	while (i < bits) {
		if (this.pos == this.data.length) {
			this.word = r
			this.bitpos = i
			return -1
		}
		r |= (this.data.charCodeAt(this.pos) & BitIO.CHARMASK) << i
		this.pos++
		i += BitIO.CHARBITS
	}
	this.word = r >> bits
	this.bitpos = i - bits
	return r & ((1 << bits) - 1)
}
