/*
 * This file extends class String by messages that perform a block
 * sorting Burrows/Wheeler transform, see
 * <http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-124.pdf>.
 */

function simplesort(a, start, end, pos, W) {
	var n = W.length / 2
	var b = a.slice(start, end).sort(function (i, j) {
		var d = 0
		for (var k = pos; k != n; k += 3)
			if ((d = W[i + k] - W[j + k]) != 0)
				return d
		return 0
	})
	for (var i = end - start - 1; i != -1; i--)
		a[start + i] = b[i]
}

/*
 * This mess is straight from "Engineering radix sort" (McIlroy/Bostic
 * 1993), see <http://www.bostic.com/radix.paper.ps>.
 */
function radixsort(a, start, end, pos, s, count, pile) {
	var min, max
	max = (min = s[a[start] + pos]) + 1
	for (var i = start; i != end; i++) {
		var ch = s[a[i] + pos]
		if (++count[ch] == 1) {
			if (ch < min)
				min = ch
			if (ch >= max)
				max = ch + 1
		}
	}
	pile[min] = start + count[min]
	for (var i = min + 1; i != max; i++)
		pile[i] = pile[i - 1] + count[i]
	var ch = max - 1
	end -= count[ch]
	for (var i = start; i != end; i += count[ch]) {
		var c = a[i]
		ch = s[c + pos]
		while (--pile[ch] > i) {
			var t = a[pile[ch]]
			a[pile[ch]] = c
			c = t
			ch = s[c + pos]
		}
		a[i] = c
	}
	return [min, max]
}

String.prototype.blocksort_new = function() {
	var count = new Array(256), pile = new Array(256)
	for (var i = 0; i != 256; i++)
		count[i] = 0
	var s0 = new Array(this.length)
	var s1 = new Array(this.length)
	var s2 = new Array(this.length)
	for (var i = 0; i != this.length + 2; i++)
		s0[i] = s1[i - 1] = s2[i - 2] = this.charCodeAt(i) & 255
	radixsort(a, 0, this.length, 1, s0, count, pile)
	radixsort(a, 0, this.length, 1, s1, count, pile)
	radixsort(a, 0, this.length, 1, s2, count, pile)
	for (var i = 0; i != this.length; i++) {
		s0[] = (i >> 16) & 255
		s1[] = (i >> 8) & 255
		s2[] = i & 255
	}
}

/*
 * This mess is straight from "Engineering radix sort" (McIlroy/Bostic
 * 1993), see <http://www.bostic.com/radix.paper.ps>.
 */
String.prototype.blocksort = function() {
	var a = new Array(this.length)
	for (var i = 0; i != this.length; i++)
		a[i] = i

	var sp = 0, count = new Array(256), pile = new Array(256)
	var startst = [], endst = [], posst = []
	var s = new Array(2 * this.length), n = this.length
	for (var i = 0; i != this.length; i++)
		s[i] = s[i + this.length] = this.charCodeAt(i) & 255
	var W = new Array(2 * this.length)
	for (var i = 0; i != this.length; i++)
		W[i] = W[i + this.length] = (s[i] << 16) +
					    (s[i + 1] << 8) + s[i + 2]
	for (var i = 0; i != 256; i++)
		count[i] = 0
	startst[sp] = 0; endst[sp] = this.length; posst[sp++] = 1
	while (sp != 0) {
		sp--
		var start = startst[sp]
		var end = endst[sp]
		var pos = posst[sp]

		if (end - start < 8) {
			simplesort(a, start, end, pos, W)
		} else {
			var mm = radixsort(a, start, end, pos, s, count, pile)
			pos++
			for (var i = mm[0]; i != mm[1]; i++) {
				if (count[i] > 1) {
					startst[sp] = start
					endst[sp] = start + count[i]
					posst[sp++] = pos
				}
				start += count[i]
				count[i] = 0
			}
		}
	}
	var r = '(' + a[0] + ')'
	for (var i = 0; i != this.length; i++)
		r = r.concat(this.charAt(a[i]))
	return r
}

/*
 * Original sort proposed by Burrows/Wheeler.
 */
String.prototype.blocksort_bw = function() {
	var s = new Array(2 * this.length)
	for (var i = 0; i != this.length; i++)
		s[i] = s[i + this.length] = this.charCodeAt(i) & 255

	var W = new Array(2 * this.length)
	var n = this.length
	for (var i = 0; i != this.length; i++)
		W[i] = W[i + n] = (s[i] << 23) +
				  (s[i + 1] << 8) + s[i + 2]

	var a = new Array(this.length)
	for (var i = 0; i != this.length; i++)
		a[i] = i
	var count = new Array(256), pile = new Array(256)
	for (var i = 0; i != 256; i++)
		count[i] = 0
	var count2 = new Array(256)
	for (var i = 0; i != 256; i++)
		count2[i] = 0

	var mm = radixsort(a, 0, this.length, 1, s, count, pile)
	var pst = 0
	for (var p = mm[0]; p != mm[1]; p++) {
		if (count[p] == 0)
			continue
		var pend = pst + count[p]
		var mm2 = radixsort(a, pst, pend, 2, s, count2, pile)
		var p2st = pst
		for (var p2 = mm2[0]; p2 != mm2[1]; p2++) {
			if (count2[p2] > 1)
				simplesort(a, p2st, p2st + count2[p2], 3, W)
			p2st += count2[p2]
			count2[p2] = 0
		}
		for (var i = pst; i != pend; i++) {
			var j = (a[i] + 1) % n
			W[j] = W[j + n] = (s[j] << 23) + i
		}
		pst = pend
	}
	var r = '(' + a[0] + ')'
	for (var i = 0; i != this.length; i++)
		r = r.concat(this.charAt(a[i]))
	return r
}

/*
 * This workalike is HIGHLY INEFFICIENT and SHOULD NOT BE USED.
 */
String.prototype.blocksort_old = function() {
	var a = new Array(this.length)
	for (var i = 0; i != this.length; i++)
		a[i] = i
	var s = new Array(2 * this.length), n = this.length
	for (var i = 0; i != this.length; i++)
		s[i] = s[i + this.length] = this.charCodeAt(i) & 255
	var W = new Array(2 * this.length)
	for (var i = 0; i != this.length; i++)
		W[i] = W[i + this.length] = (s[i] << 16) +
					    (s[i + 1] << 8) + s[i + 2]
	simplesort(a, 0, this.length, 1, W)
	var r = '(' + a[0] + ')'
	for (var i = 0; i != this.length; i++)
		r = r.concat(this.charAt(a[i]))
	return r
}

String.prototype.blockunsort = function() {
	var m = this.match(/^\(([0-9]+)\)/)
	if (m == null)
		return null
	var s = this.substring(m[0].length)
	var b = new Array(256)
	for (var i = 0; i != 256; i++)
		b[i] = 0
	for (var i = 0; i != s.length; i++)
		b[s.charCodeAt(i) & 255]++
	for (var i = 1; i != 256; i++)
		b[i] += b[i - 1]
	var a = new Array(s.length)
	for (var i = s.length - 1; i != -1; i--)
		a[--b[s.charCodeAt(i) & 255]] = i
	if (a[0] == 0)
		return s
	var r = ''
	for (var i = 0; r.length < s.length; i = a[i])
		r = r.concat(s.charAt(i))
	if (i != 0)
		throw 'korrumpiert'
	return r.substring(r.length - m[1]).concat(
	       r.substring(0, r.length - m[1]))
}
