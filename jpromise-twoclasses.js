var JPromise = (function () {
    "use strict";

    function callable(obj) {
        return "function" === typeof obj ||
               null !== obj && "object" === typeof obj && obj instanceof Function;
    }

    function thenable(obj) {
        return null !== obj &&
               ("object" === typeof obj || "function" === typeof obj) &&
               callable(obj.then);
    }

    var tasks = [];
    function queue(task) {
        if (!tasks.length) {
            window.setTimeout(function () {
                var t = tasks;
                tasks = [];
                t.forEach(function (task) { task(); });
            });
        }
        tasks.push(task);
    }

    function perform(resolve, reject, onSuccess, onFailure, fulfilled, value) {
        var f = fulfilled ? onSuccess : onFailure;
        if (callable(f)) {
            try {
                resolve(f(value));
            } catch (reason) {
                reject(reason);
            }
        } else {
            (fulfilled ? resolve : reject)(value);
        }
    }

    /**
     * I represent the Promise's fulfilled and rejected states.
     */
    function State(fulfilled, value) {
        this.fulfilled = fulfilled;
        this.value = value;
    }

    State.prototype.resolve = function () {};
    State.prototype.then = function (onSuccess, onFailure) {
        var fulfilled = this.fulfilled;
        var value = this.value;
        return new JPromise(function (resolve, reject) {
            queue(function () { perform(resolve, reject, onSuccess, onFailure, fulfilled, value); });
        });
    };

    /**
     * I represent the Promise's pending state. I replace myself with an
     * instance of State when the Promise settles.
     */
    function PendingState(setState) {
        this.setState = setState;
        this.then = [];
    }

    PendingState.prototype.settle = function (fulfilled, value) {
        var then = this.then;
        this.then = [];
        this.setState(new State(fulfilled, value));
        this.settle = this.setState = function () {};
        if (0 < then.length) {
            queue(function () {
                then.forEach(function (record) {
                    perform(record.resolve, record.reject, record.onSuccess, record.onFailure, fulfilled, value);
                });
            });
        }
    }

    PendingState.prototype.resolve = function (fulfilled, value) {
        var self = this;
        self.resolve = function () {};
        if (fulfilled && thenable(value)) {
            value.then(function (value) {
                self.settle(true, value);
            }, function (reason) {
                self.settle(false, reason);
            });
        } else {
            self.settle(fulfilled, value);
        }
    };

    PendingState.prototype.then = function (onSuccess, onFailure) {
        var self = this;
        return new JPromise(function (resolve, reject) {
            self.then.push({
                "resolve": resolve, "reject": reject,
                "onSuccess": onSuccess, "onFailure": onFailure
            });
        });
    };

    function self(ctor) {
        if (!(this instanceof JPromise)) {
            throw new Error("JPromise must be called with 'new'");
        }

        var state = new PendingState(function (newState) { state = newState; });
        this.then = function (onSuccess, onFailure) {
            return state.then(onSuccess, onFailure);
        };

        try {
            ctor(function (value) { state.resolve(true, value); },
                 function (reason) { state.resolve(false, reason); });
        } catch (reason) {
            state.resolve(false, reason);
        }
    }

    self.resolve = function (value) {
        if (value instanceof JPromise) {
            return value;
        }

        return new JPromise(function (resolve, reject) { resolve(value); });
    };

    self.reject = function (reason) {
        return new JPromise(function (resolve, reject) { reject(reason); });
    };

    // self.all
    // self.race

    return self;
}());

JPromise.prototype.catch = function (onFailure) {
    "use strict";
    return this.then(undefined, onFailure);
};
