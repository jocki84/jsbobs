const JPromise = require('../jpromise.js');

console.log('resolved subclass', JPromise.resolve() instanceof JPromise);
console.log('rejected subclass', JPromise.reject() instanceof JPromise);

// If/when `resolvePromise` is called with a value `y`, run `[[Resolve]](promise, y)`
JPromise.resolve().then(function () {
    // `y` is a thenable that fulfills but then throws for an asynchronously-fulfilled custom thenable
    return {
        then(onFulfilled0) {
            // calls resolvePromise synchronously
            onFulfilled0({
                then(onFulfilled1) {
                    // asynchronously-fulfilled custom thenable
                    setTimeout(onFulfilled1, 50);
                },
            });
            // but then throws
            throw {};
        },
    };
});

const prom0 = Promise.resolve().then(function () {
    return {
        then(onFulfilled, onRejected) {
            //onFulfilled(new Promise(function () {}));
            onFulfilled('overtake');
            onRejected('overtake');
        },
    };
});
prom0.then(console.log);

JPromise.resolve({}).then(function () {
    return Object.create(null, {
	'then': {
	    get() {
		console.log('then gotten');
		return function (onFulfilled) {
		    onFulfilled('value');
		}
	    },
	},
    });
}).then(console.log);
