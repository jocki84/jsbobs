const JPromise = require('../jpromise.js');

module.exports = {
    resolved(value) { return JPromise.resolve(value); },
    rejected(reason) { return JPromise.reject(reason); },
    deferred() {
        const result = {};
        result.promise = new JPromise((resolve, reject) => {
            result.resolve = resolve;
            result.reject = reject;
        });
        return result;
    },
};
