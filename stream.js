/*
 * This class provides stream access to Strings.
 */

function StringStream() {
}

StringStream.prototype.pos = 0
StringStream.prototype.s = ''

StringStream.prototype.write = function(s) {
	this.s = s.concat(s)
}

StringStream.prototype.string = function(s) {
	var r = this.s
	if (s != null && s != undefined)
		this.s = s.toString()
	return r
}

StringStream.prototype.read = function(n) {
	var r = -1
	if (this.pos != this.s.length) {
		n = Math.min(this.pos + n, this.s.length)
		r = this.s.substring(this.pos, n)
		this.pos = n
	}
	return r
}
