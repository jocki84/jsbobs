{-# LANGUAGE DefaultSignatures #-}

import Prelude((.), ($), const, flip, id)

{-
 - Class definitions
 -}

class Functor f where
    (<$>) :: (a -> b) -> f a -> f b

    fmap :: (a -> b) -> f a -> f b
    fmap = (<$>)

    (<$) :: a -> f b -> f a
    (<$) = fmap . const

    ($>) :: f a -> b -> f b
    ($>) = flip (<$)

class (Functor f) => Apply f where
    (<*>) :: f (a -> b) -> f a -> f b

    default (<*>) :: (Bind f) => f (a -> b) -> f a -> f b
    f <*> a = f >>= \f' -> fmap f' a

    ap :: f (a -> b) -> f a -> f b
    ap = (<*>)

    (<*) :: f a -> f b -> f a
    a <* b = const <$> a <*> b

    (*>) :: f a -> f b -> f b
    a *> b = const id <$> a <*> b

liftA2 :: Apply f => (a -> b -> c) -> f a -> f b -> f c
liftA2 f a b = f <$> a <*> b

class (Apply f) => Applicative f where
    return :: a -> f a

class (Apply m) => Bind m where
    (>>=) :: m a -> (a -> m b) -> m b

    (=<<) :: (a -> m b) -> m a -> m b
    (=<<) = flip (>>=)

    (>>) :: m a -> m b -> m b
    a >> b = a >>= const b

class (Applicative m, Bind m) => Monad m

{-
 - Result
 -}

data Result e a = Err e | Ok a

either :: (e -> b) -> (a -> b) -> (Result e a) -> b
either f _ (Err e) = f e
either _ f (Ok a) = f a

instance Functor (Result e) where
    _ <$> Err e = Err e
    f <$> Ok a = Ok $ f a

instance Apply (Result e)

instance Applicative (Result e) where
    return = Ok

instance Bind (Result e) where
    Err e >>= _ = Err e
    Ok a >>= f = f a

instance Monad (Result e)

{-
 - ResultT
 -}

newtype ResultT e m a = ResultT { runResultT :: m (Result e a) }

instance Functor m => Functor (ResultT e m) where
    (<$>) f = ResultT . fmap (fmap f) . runResultT

instance Apply m => Apply (ResultT e m) where
    -- fmap ap f :: m (Result e a -> Result e b)
    ResultT f <*> ResultT a = ResultT $ fmap ap f <*> a

instance Applicative m => Applicative (ResultT e m) where
    return = ResultT . return . return

instance (Monad m) => Bind (ResultT e m) where
    ResultT x >>= f = ResultT $ x >>= either (\e -> return $ Err e)
                                             (runResultT . f)

instance (Monad m) => Monad (ResultT e m)

{-
 - Async
 -}

newtype Async a = Async { runAsync :: (a -> ()) -> () }

instance Functor Async where
    f <$> Async a = Async $ \cb -> a $ cb . f

-- f <*> a = AsyncResult $ (runAsyncResult f) >>= \f' ->
--                         (runAsyncResult a) >>= \a' -> return $ f' <*> a'
instance Apply Async

instance Applicative Async where
    return a = Async ($ a)

instance Bind Async where
    Async a >>= f = Async $ \cb -> a $ \a' -> runAsync (f a') cb

instance Monad Async

{-
 - AsyncResult
 -}

type AsyncResult e = ResultT e Async
