/*
 * Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the
 *      distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * An algebraic data type representing asynchronous results.
 *
 *        newtype Async a = Async { runAsync :: (a -> ()) -> () }
 *
 * The Async monad consists of asynchronous values. An asynchronous value
 * is a function that takes a callback. If an asynchronous value evaluates,
 * the callback is invoked with the value.
 *
 * An asynchronous function is a function that yields an asynchronous value.
 *
 * For an asynchronous function f, Async.chain(f) returns an asynchronous
 * function that takes an asynchronous value and evaluates f on it.
 *
 * For an ordinary value a, Async.of(a) yields an asynchronous value that
 * calls its callback with a.
 *
 * For an asynchronous function f we have
 *
 *              Async.chain(f) ∘ Async.of = Async.chain(Async.of) ∘ f = f.
 *
 * For ordinary functions g and h we have
 *
 *              Async.chain(Async.of ∘ g) ∘ Async.of ∘ h = Async.of ∘ g ∘ h.
 */

function checkFunction(f) {
    if ("function" !== typeof f) {
        throw new TypeError("Async: function required");
    }
    return f;
}

function checkAsync(a) {
    if ("object" !== typeof a || "function" !== typeof a.runAsync) {
        throw new TypeError("Async: Async required");
    }
    return a;
}

// newtype Async a = Async { runAsync :: (a -> ()) -> () }
function async(runAsync) {
    checkFunction(runAsync);

    // map :: Async a ~> (a -> b) -> Async b
    function map(f) {
        checkFunction(f);

        return async(function (cb) {
            checkFunction(cb);

            return runAsync((aa) => cb(f(aa)));
        });
    }

    // ap :: Async a ~> Async (a -> b) -> Async b
    function ap(f) {
        return checkAsync(f).chain(map);
    }

    // chain :: Async a ~> (a -> Async b) -> Async b
    function chain(f) {
        checkFunction(f);

        return async(function (cb) {
            checkFunction(cb);

            return runAsync((aa) => checkAsync(f(aa)).runAsync(cb));
        });
    }

    // andThen :: Async a ~> Async b -> Async b
    function andThen(b) {
        checkAsync(b);

        return chain(() => b);
    }

    return {andThen, ap, chain, map, runAsync};
}

async.isA = checkAsync;

// of :: a -> Async a
async.of = (a) => async((cb) => cb(a));

export default async;
