/*
 * Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the
 *      distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import {assert, assertEq, assertExc} from "./assert.js";
import Result from "./Result.js";
import async from "./Async.js";
import resultT from "./ResultT.js";

// type AsyncResult e = ResultT e Async
const asyncResult = resultT(async);

/*
 *
 * Test utilities
 *
 */

let actionCount = 0;
let valueCount = 0;

function testAction(expected, name, valueFactory) {
    return function (a) {
        actionCount += 1;
        assertEq(expected, a, name, "action");
        return valueFactory();
    };
}

function testValue() {
    valueCount += 1;
    return `count ${valueCount}`;
}

function asyncValue() {
    return async(function (cb) {
        cb(testValue());
    });
}

function resultAction(expected, name, result) {
    return function (a) {
        actionCount += 1;
        assertEq(expected, a, name, "action");
        return result;
    };
}

function errValue() {
    return asyncResult(async(function (cb) {
        valueCount += 1;
        cb(Result.err(`count ${valueCount}`));
    }));
}

function okValue() {
    return asyncResult(async(function (cb) {
        valueCount += 1;
        cb(Result.ok(`count ${valueCount}`));
    }));
}

/*
 *
 * Async
 *
 */

// Throw if not function.
assertExc(TypeError, () => async(123), "at lifecycle");

// Invoke single action.
const at0 = asyncValue();

actionCount = 0;
valueCount = 0;
at0.runAsync(function (a) {
    actionCount += 1;
    assertEq("count 1", a, "at0 result");
});
assertEq(1, actionCount, "at0 actionCount");
assertEq(1, valueCount, "at0 valueCount");

// Invoke chained actions.
const at1 = asyncValue().chain(
    testAction("count 1", "at1 first", asyncValue)
).chain(
    testAction("count 2", "at1 second", asyncValue)
).chain(
    testAction("count 3", "at1 third", asyncValue)
).chain(
    testAction("count 4", "at1 fourth", asyncValue)
).chain(
    testAction("count 5", "at1 fifth", asyncValue)
).chain(
    testAction("count 6", "at1 sixth", asyncValue)
);

actionCount = 0;
valueCount = 0;
at1.runAsync(function (a) {
    actionCount += 1;
    assertEq("count 7", a, "at1 result");
});
assertEq(7, actionCount, "at1 actionCount");
assertEq(7, valueCount, "at1 valueCount");

// Invoke andThen'd actions.
const at2 = asyncValue().andThen(
    asyncValue()
).andThen(
    asyncValue()
).andThen(
    asyncValue()
).andThen(
    asyncValue()
).andThen(
    asyncValue()
).andThen(
    asyncValue()
);

actionCount = 0;
valueCount = 0;
at2.runAsync(function (a) {
    actionCount += 1;
    assertEq("count 7", a, "at2 result");
});
assertEq(1, actionCount, "at2 actionCount");
assertEq(7, valueCount, "at2 valueCount");

// Test synchronous action.
const at3 = asyncValue().map(
    testAction("count 1", "at3 first", testValue)
).map(
    testAction("count 2", "at3 second", testValue)
).map(
    testAction("count 3", "at3 third", testValue)
).map(
    testAction("count 4", "at3 fourth", testValue)
).map(
    testAction("count 5", "at3 fifth", testValue)
).map(
    testAction("count 6", "at3 sixth", testValue)
);

actionCount = 0;
valueCount = 0;
at3.runAsync(function (a) {
    actionCount += 1;
    assertEq("count 7", a, "at3 result", a);
});
assertEq(7, actionCount, "at3 actionCount");
assertEq(7, valueCount, "at3 valueCount");

/*
 *
 * AsyncResult
 *
 */

// Throw if not async.
assertExc(TypeError, () => asyncResult(() => undefined), "art lifecycle");

// Invoke single action.
const art0 = okValue();

actionCount = 0;
valueCount = 0;
art0.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assert(false, "art0 failed with", e);
        },
        function (a) {
            assertEq("count 1", a, "art0 result");
        }
    );
});
assertEq(1, actionCount, "art0 actionCount");
assertEq(1, valueCount, "art0 valueCount");

// Throw if does not return Result.
const art1 = asyncResult(async(function (cb) {
    valueCount += 1;
    cb("count " + valueCount);
})).chain(
    resultAction("count 1", "art1 first", okValue())
);

actionCount = 0;
valueCount = 0;
assertExc(
    TypeError,
    () => art1.runResultT.runAsync(function () {
        actionCount += 1;
    }),
    "art1 exception"
);
assertEq(0, actionCount, "art1 actionCount");
assertEq(1, valueCount, "art1 valueCount");

// Invoke chained actions.
const art2 = okValue().chain(
    resultAction("count 1", "art2 first", okValue())
).chain(
    resultAction("count 2", "art2 second", okValue())
).chain(
    resultAction("count 3", "art2 third", okValue())
).chain(
    resultAction("count 4", "art2 fourth", okValue())
).chain(
    resultAction("count 5", "art2 fifth", okValue())
).chain(
    resultAction("count 6", "art2 sixth", okValue())
);

actionCount = 0;
valueCount = 0;
art2.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assert(false, "art2 failed with", e);
        },
        function (a) {
            assertEq("count 7", a, "art2 result");
        }
    );
});
assertEq(7, actionCount, "art2 actionCount");
assertEq(7, valueCount, "art2 valueCount");

// Invoke andThen'd actions.
const art3 = okValue().andThen(
    okValue()
).andThen(
    okValue()
).andThen(
    okValue()
).andThen(
    okValue()
).andThen(
    okValue()
).andThen(
    okValue()
);

actionCount = 0;
valueCount = 0;
art3.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assert(false, "art3 failed with", e);
        },
        function (a) {
            assertEq("count 7", a, "art3 result");
        }
    );
});
assertEq(1, actionCount, "art3 actionCount");
assertEq(7, valueCount, "art3 valueCount");

// Fail along the chain.
const art4 = okValue().chain(
    resultAction("count 1", "art4 first", errValue())
).chain(
    resultAction("count 2", "art4 second", okValue())
).chain(
    resultAction("count 3", "art4 third", okValue())
).chain(
    resultAction("count 4", "art4 fourth", okValue())
).chain(
    resultAction("count 5", "art4 fifth", okValue())
).chain(
    resultAction("count 6", "art4 sixth", okValue())
);

actionCount = 0;
valueCount = 0;
art4.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assertEq("count 2", e, "art4 result");
        },
        function (a) {
            assert(false, "art4 succeeded with", a);
        }
    );
});
assertEq(2, actionCount, "art4 actionCount");
assertEq(2, valueCount, "art4 valueCount");

// Fail along the andThen.
const art5 = okValue().andThen(
    errValue()
).andThen(
    okValue()
);

actionCount = 0;
valueCount = 0;
art5.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assertEq("count 2", e, "art5 result");
        },
        function (a) {
            assert(false, "art5 succeeded with", a);
        }
    );
});
assertEq(1, actionCount, "art5 actionCount");
assertEq(2, valueCount, "art5 valueCount");

// Test synchronous action.
const art6 = okValue().map(
    testAction("count 1", "art6 first", testValue)
).map(
    testAction("count 2", "art6 second", testValue)
).map(
    testAction("count 3", "art6 third", testValue)
).map(
    testAction("count 4", "art6 fourth", testValue)
).map(
    testAction("count 5", "art6 fifth", testValue)
).map(
    testAction("count 6", "art6 sixth", testValue)
);

actionCount = 0;
valueCount = 0;
art6.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        function (e) {
            assert(false, "art6 failed with", e);
        },
        function (a) {
            assertEq("count 7", a, "art6 result");
        }
    );
});
assertEq(7, actionCount, "art6 actionCount");
assertEq(7, valueCount, "art6 valueCount");

// Throw converted to Result.err.
const art7 = okValue().map(
    function (a) {
        actionCount += 1;
        assertEq("count 1", a, "art7 first action");
        throw new Error(testValue());
    }
).map(
    testAction("count 2", "art7 second", testValue)
).map(
    testAction("count 3", "art7 third", testValue)
).map(
    testAction("count 4", "art7 fourth", testValue)
).map(
    testAction("count 5", "art7 fifth", testValue)
).map(
    testAction("count 6", "art7 sixth", testValue)
);

actionCount = 0;
valueCount = 0;
art7.runResultT.runAsync(function (r) {
    actionCount += 1;
    r.either(
        (e) => assert(
            Error.prototype === Object.getPrototypeOf(e),
            "art7 result expected Error actual",
            Object.getPrototypeOf(e).constructor.name
        ),
        (a) => assert(false, "art7 succeeded with", a)
    );
});
assertEq(2, actionCount, "art7 actionCount");
assertEq(2, valueCount, "art7 valueCount");
