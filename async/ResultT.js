/*
 * Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the
 *      distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * A monad transformer that allows wrapping a monad around the Result type
 * to represent failure or success.
 *
 *        newtype ResultT e m a = ResultT { runResultT :: m (Result e a) }
 */

import Result from "./Result.js";

function checkFunction(f) {
    if ("function" !== typeof f) {
        throw new TypeError("ResultT: function required");
    }
    return f;
}

export default function resultT(M) {
    if (!M || "function" !== typeof M.of) {
        throw new TypeError("ResultT: Monad required");
    }

    function checkResultT(r) {
        if ("object" !== typeof r) {
            throw new TypeError("ResultT: ResultT required");
        }
        M.isA(r.runResultT);
        return r;
    }

    // newtype ResultT e m a = ResultT { runResultT :: m (Result e a) }
    function result(runResultT) {
        M.isA(runResultT);

        // map :: ResultT e m a ~> (a -> b) -> ResultT e m b
        function map(f) {
            checkFunction(f);

            return result(runResultT.map((r) => Result.isA(r).map(f)));
        }

        // ap :: ResultT e m a ~> ResultT e m (a -> b) -> ResultT e m b
        function ap(f) {
            checkResultT(f);

            /*
             * Transform
             *        m (Result e a)    ~> m (Result e (a -> b))        -> m (Result e b)
             * using m's map into
             *        m (Result e a)    ~> m (Result e a -> Result e b) -> m (Result e b)
             * using Result's ap which is
             *        ap :: Result e a  ~> Result e (a -> b)            -> Result e b
             */
            return result(runResultT.ap(f.runResultT.map(function (ff) {
                // ff :: Result e (a -> b)
                Result.isA(ff);

                // Result e a -> Result e b
                return (a) => Result.isA(a).ap(ff);
            })));
        }

        // chain :: ResultT e m a ~> (a -> ResultT e m b) -> ResultT e m b
        function chain(f) {
            checkFunction(f);

            return result(runResultT.chain(
                (r) => r.either(
                    (e) => M.of(Result.err(e)),
                    (a) => checkResultT(f(a)).runResultT
                )
            ));
        }

        // andThen :: ResultT e m a ~> ResultT e m b -> ResultT e m b
        function andThen(b) {
            checkResultT(b);

            return chain(() => b);
        }

        // tap :: ResultT e m a ~> (a -> b) -> ResultT e m a
        function tap(f) {
            checkFunction(f);

            return map(function (a) {
                try {
                    f(a);
                } catch (ignore) {
                }
                return a;
            });
        }

        return {andThen, ap, chain, map, runResultT, tap};
    }

    result.isA = checkResultT;

    result.of = (a) => result(M.of(Result.ok(a)));

    return result;
}
