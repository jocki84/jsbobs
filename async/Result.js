/*
 * Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the
 *      distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * An algebraic data type with two constructors which can be used to
 * represent the failure or success of a computation.
 *
 *        data Result e a = Err e | Ok a
 */

function checkResult(r) {
    // Note: presence of "either" is just a heuristic.
    if ("object" !== typeof r || "function" !== typeof r.either) {
        throw new TypeError("Result: Result required");
    }
    return r;
}

// Err :: e -> Result e a
function err(e) {
    const self = {};

    // map :: Result e a ~> (a -> b) -> Result e b
    // ap :: Result e a ~> (a -> Result e b) -> Result e b
    // chain :: Result e a ~> (a -> Result e b) -> Result e b
    const pass = () => self;

    // either :: Result e a ~> (e -> b) -> (a -> b) -> b
    const either = (f, ignore) => f(e);

    self.ap = pass;
    self.chain = pass;
    self.either = either;
    self.map = pass;
    return self;
}

// Ok :: a -> Result e a
function ok(a) {
    // map :: Result e a ~> (a -> b) -> Result e b
    function map(f) {
        try {
            return ok(f(a));
        } catch (exc) {
            return err(exc);
        }
    }

    // ap :: Result e a ~> Result e (a -> b) -> Result e b
    const ap = (f) => checkResult(f).chain(map);

    // chain :: Result e a ~> (a -> Result e b) -> Result e b
    const chain = (f) => checkResult(f(a));

    // either :: Result e a ~> (e -> b) -> (a -> b) -> b
    const either = (ignore, f) => f(a);

    return {ap, chain, either, map};
}

export default {err, isA: checkResult, of: ok, ok};
