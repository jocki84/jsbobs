/*
 * This file provides class State which represents the state of a
 * table-driven compression algorithm.
 *
 * We use an instance of class BitIO for input from and output to a String.
 */

function State(bits, blockmode) {
	if (!this.setbits(bits))
		this.setbits(State.MAXBITS)
	this.blockmode = !!blockmode
	this.bitio = new BitIO()
	this.reset()
}

State.MINBITS = BitIO.CHARBITS + 1
State.IC = 1 << BitIO.CHARBITS
State.MAXBITS = 24
State.MAGIC = 0x9d1f

State.prototype.prefix = null
State.prototype.bits = State.MINBITS
State.prototype.maxcode = 1 << State.prototype.bits
State.prototype.maxbits = State.MAXBITS
State.prototype.maxmaxcode = 1 << State.MAXBITS
State.prototype.words = 0
State.prototype.n = State.IC
State.prototype.tab = []
State.prototype.inbits = 0
State.prototype.outbits = 0
State.prototype.ratio = 0

State.prototype.setbits = function(bits) {
	if (bits >= State.MINBITS && bits <= State.MAXBITS) {
		this.maxbits = bits
		this.maxmaxcode = 1 << bits
		return true
	}
	return false
}

/*
 * Only call when blockmode == true and words % BitIO.CHARBITS == 0.
 */
State.prototype.reset = function() {
	this.bits = State.MINBITS
	this.maxcode = 1 << this.bits
	this.n = State.IC + (this.blockmode ? 1 : 0)
	this.tab = []
	this.inbits = this.outbits = this.ratio = 0
}

State.prototype.reduce = function(c) {
	var hash = (this.prefix << BitIO.CHARBITS) + c
	if (this.tab.hasOwnProperty(hash)) {
		this.prefix = this.tab[hash]
		return
	}
	this.bitio.append(this.prefix, this.bits)
	this.words++
	this.outbits += this.bits
	this.prefix = c
	if (this.n != this.maxmaxcode) {
		this.tab[hash] = this.n
		if (this.n == this.maxcode) {
			while (this.words & (BitIO.CHARBITS - 1) != 0) {
				this.bitio.append(0, this.bits)
				this.words++
			}
			this.bits++
			this.maxcode <<= 1
		}
		this.n++
	}
	if (this.blockmode && ((this.words + 1) & (this.maxmaxcode - 1)) == 0) {
		if (this.inbits / this.outbits < this.ratio) {
			this.bitio.append(State.IC, this.bits)
			this.words++
			this.reset()
		} else {
			this.ratio = this.inbits / this.outbits
		}
	}
}

State.prototype.extend = function(code) {
	var s = this.chars(code)
	if (this.prefix != null && this.n != this.maxmaxcode) {
		this.tab[this.n] = (this.prefix << BitIO.CHARBITS) +
				   s.charCodeAt(0)
		this.n++
		if (this.n == this.maxcode && this.n != this.maxmaxcode) {
			while (this.words & (BitIO.CHARBITS - 1) != 0) {
				this.bitio.get(this.bits)
				this.words++
			}
			this.bits++
			this.maxcode <<= 1
		}
	}
	this.prefix = code
	return s
}

State.prototype.chars = function(c) {
	var r = '', last = false
	if (c == this.n) {
		c = this.prefix
		last = true
	}
	while (c >= State.IC) {
		c = this.tab[c]
		r = String.fromCharCode(c & BitIO.CHARMASK).concat(r)
		c >>>= BitIO.CHARBITS
	}
	var ch = String.fromCharCode(c)
	if (last)
		r = r.concat(ch)
	return ch.concat(r)
}

State.prototype.komp = function(s) {
	for (var i = 0; i != s.length; i++) {
		var c = s.charCodeAt(i) & BitIO.CHARMASK
		this.inbits += BitIO.CHARBITS
		if (this.prefix == null) {
			this.bitio.append(State.MAGIC, 16)
			this.bitio.append(this.maxbits, 5)
			this.bitio.append(0, 2)
			this.bitio.append(this.blockmode ? 1 : 0, 1)
			this.prefix = c
		} else {
			this.reduce(c)
		}
	}
	return true
}

State.prototype.close = function() {
	if (this.prefix != null) {
		this.bitio.append(this.prefix, this.bits)
		this.bitio.close()
		this.prefix = null
	}
	return this.bitio.data
}

State.prototype.dekomp = function(s) {
	this.bitio.source(s)
	if (this.words == 0) {
		if (this.bitio.get(16) != State.MAGIC)
			return null
		if (!this.setbits(this.bitio.get(5)))
			throw 'can only handle ' + State.MINBITS +
			      ' to ' + State.MAXBITS + ' bits'
		this.bitio.get(2) // ignore reserved
		this.blockmode = (this.bitio.get(1) != 0)
		this.reset()
	}

	var c, r = ''
	while ((c = this.bitio.get(this.bits)) != -1) {
		this.words++
		if (c > this.n) {
			throw 'korrumpiert'
		} else if (this.blockmode && c == State.IC) {
			while (this.words & (BitIO.CHARBITS - 1) != 0) {
				this.bitio.get(this.bits)
				this.words++
			}
			this.reset()
			this.prefix = null
		} else {
			r = r.concat(this.extend(c))
		}
	}
	return r
}
