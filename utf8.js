/*
 * This file adds messages to class String that handle conversion from
 * Unicode to the UTF-8 encoding described in utf(6) of the Plan 9 manual,
 * see <http://cm.bell-labs.com/magic/man2html/6/utf>.
 */

String.prototype.encode_utf8 = function() {
	var r = ''
	for (var i = 0; i != this.length; i++) {
		var t = []
		t[0] = this.charCodeAt(i)
		if (t[0] < 0x80) {
			r = r.concat(this.charAt(i))
		} else {
			t[1] = t[0] >> 6
			t[0] &= 0x3f
			t[2] = t[1] >> 6
			t[1] &= 0x3f
			if (t[1] < 0x20)
				r = r.concat(String.fromCharCode(0xc0 | t[1]))
			else
				r = r.concat(String.fromCharCode(0xe0 | t[2])).
				      concat(String.fromCharCode(0x80 | t[1]))
			r = r.concat(String.fromCharCode(0x80 | t[0]))
		}
	}
	return r
}

String.prototype.decode_utf8 = function() {
	var r = ''
	for (var i = 0; i != this.length; i++) {
		var c = this.charCodeAt(i)
		if (c < 0x80) {
		} else if (0xc2 <= c && c < 0xe0 && i + 1 < this.length) {
			c = (c & 0x1f) << 6
			i++
			c |= this.charCodeAt(i) & 0x3f
		} else if (0xe0 <= c && c < 0xf0 && i + 2 < this.length) {
			c = (c & 0xf) << 6
			i++
			c |= this.charCodeAt(i) & 0x3f
			c <<= 6
			i++
			c |= this.charCodeAt(i) & 0x3f
		} else {
			c = 0xfffd
		}
		r = r.concat(String.fromCharCode(c))
	}
	return r
}
