/**
 * This module implements the Async monad.
 *
 * The Async monad consists of asynchronous values, also called *async*.
 *
 * * An asynchronous value is a function from a callback to an optional
 *   cancellation function.
 * * A callback is a function with two arguments (success, failure).
 * * An asynchronous function is a function that evaluates to an
 *   asynchrorous value.
 *
 * For two ordinary functions f and g we have that
 *
 *              sequence(lift(f), lift(g)) = lift(f ∘ g)
 *
 * For any asynchronous function h we have that
 *
 *              sequence(h, unit) = sequence(unit, h) = h
 *
 * @module
 */

const invokeOnce = function (f) {
    let alreadyInvoked = false;
    let success;
    let failure;

    return function (...as) {
        if (!alreadyInvoked) {
            try {
                success = f(...as);
            } catch (exc) {
                failure = exc;
            }
            alreadyInvoked = true;
        }

        if (failure) {
            throw failure;
        }
        return success;
    };
};

const cancelAll = function (cancels) {
    cancels.forEach(function (cancel) {
        if ("function" === typeof cancel) {
            try {
                cancel();
            } catch (ignore) {
            }
        }
    });
    cancels.length = 0;
};

/**
 * If f, g are asynchronous functions, then bind(f) ∘ g is an asynchronous
 * function representing the composition of f and g. Conceptually,
 * bind(f) ∘ g applies f to the result g.
 *
 * @param f An asynchronous function.
 * @return A function that applies f to an async value.
 */
const bind = (f) => (async) => function (cb) {
    const cancels = [];
    let finished = false;

    cancels[0] = async(function (success, failure) {
        cancels.length = 0;
        if (finished) {
            return;
        }

        if (failure) {
            return cb(success, failure);
        }

        cancels[0] = f(success)(cb);
    });

    return function () {
        finished = true;
        cancelAll(cancels);
    };
};

/**
 * @param asyncs An arbitrary number of asyncs.
 * @return An async for the first async that succeeds.
 */
const fallback = (...asyncs) => function (cb) {
    const cancels = [];
    let finished = false;

    asyncs.reduceRight(
        (cb, async) => function (success, failure) {
            cancels.length = 0;
            if (finished) {
                return;
            }

            if (!failure) {
                return cb(success, failure);
            }

            cancels[0] = async(cb);
        },
        cb
    )(undefined, new Error("Empty fallback sequence."));

    return function () {
        finished = true;
        cancelAll(cancels);
    };
};

/**
 * @param h A function.
 * @return An asynchronous function which evaluates h.
 */
const lift = (h) => (...as) => function (cb) {
    try {
        cb(h(...as));
    } catch (failure) {
        cb(undefined, failure);
    }
};

/**
 * @param asyncs An arbitrary number of asyncs.
 * @return An async for an array of results.
 */
const parallel = (...asyncs) => function (cb) {
    let resultsOutstanding = asyncs.length;
    const results = [];
    const cancels = [];

    cancels.concat(
        asyncs.map((async, i) => async(invokeOnce(function (success, failure) {
            cancels[i] = undefined;
            if (0 === resultsOutstanding) {
                return;
            }

            if (failure) {
                resultsOutstanding = 0;
                results.length = 0;
                cancelAll(cancels);
                return cb(undefined, failure);
            }

            results[i] = success;
            resultsOutstanding -= 1;

            if (0 === resultsOutstanding) {
                cancels.length = 0;
                return cb(results);
            }
        })))
    );

    return function () {
        resultsOutstanding = 0;
        results.length = 0;
        cancelAll(cancels);
    };
};

/**
 * @param asyncs An arbitrary number of asyncs.
 * @return An async for the first async that succeeds or fails.
 */
const race = (...asyncs) => function (cb) {
    const cancels = [];
    let finished = false;

    cancels.concat(
        asyncs.map((async) => async(function (success, failure) {
            if (finished) {
                return;
            }
            finished = true;

            cancelAll(cancels);
            cb(success, failure);
        }))
    );

    return function () {
        finished = true;
        cancelAll(cancels);
    };
};

/**
 * unit is an asynchronous function that invokes its callback with the
 * argument.
 *
 * @param a A value.
 * @return An async for the value a.
 */
const unit = (a) => (cb) => cb(a);

/**
 * As a convenience, sequence(f, g) := bind(f) ∘ g composes two asynchronous
 * functions, and lift(h) := unit ∘ h turns an ordinary function h into an
 * asynchronous function.
 *
 * @param fs A sequence of asynchronous functions to be executed
 * sequentially.
 * @return An asynchronous function for the function sequence composition.
 */
const sequence = (...fs) => fs.reduce((g, f) => (a) => bind(f)(g(a)), unit);

/**
 * The callback accepted by an async.
 *
 * @callback callback
 * @param success The value of the async.
 * @param failure If truthy, the failure that occurred.
 */

export default {bind, fallback, lift, parallel, race, sequence, unit};
